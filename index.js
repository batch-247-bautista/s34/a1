// 1. Import express using require directive
const express = require("express");

// 2. Initialize express by using it to the app variable as a function
const app = express();

// 3. Set the port number
const port = 3000;

// 4. Use middleware to allow express to lead JSON
app.use(express.json());

// 5.  Use middleware to allow express to be able to read more data types from a response
app.use(express.urlencoded({extended: true}))

// 6. Listen to the port and console.log a text once the server is running
app.listen(port, () => console.log(`Server is running at localhost: ${port}`));




// Homepage message

	app.get("/homepage", (req, res) => {
		res.send('Welcome to the home page');
	});

	let users = [{
      "username": "JohnL",
      "password": "imagn3"
   },
   {
      "username": "PaulM",
      "password": "Livndi3"
   },
   {
      "username": "GeorgH",
      "password":"imagn3"
   },
    {
      "username": "RingoS",
      "password":"Actn2rly"
   },
   ];

	
   
// DISPLAY users array

app.get('/users', (req,res)=>{
 
  res.send(users);
});










// DELETE a username

app.delete("/delete-user", (req, res) => {
    // Creates a variable to store the message to be sent back to the client/Postman
    let message;

     for(let i = 0; i < users.length; i++){
      
      if(req.body.username == users[i].username){

        users[i].password = req.body.password;
  
        message = `User ${req.body.username} has been deleted`;

      break;

      
      } else {
        message = "User does not exist!";
    } 
  }
    res.send(message);
  })


     